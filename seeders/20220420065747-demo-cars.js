"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("Cars", [
      {
        name: "Avanza",
        type: "Home Car",
        price: "450000",
        image: "avanza.png",
        size: "Medium",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ferrari",
        type: "Sport Car",
        price: "950000",
        image: "ferrari.jpg",
        size: "Medium",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Hummer",
        type: "Trail Car",
        price: "700000",
        image: "hummer.jpg",
        size: "Large",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Cars", null, {});
  },
};