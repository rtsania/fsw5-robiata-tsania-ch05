# BINAR FSW5 Challenge-05 Kelompok 3 

## Anggota Kelompok 
1. Muhamad Arif Hidayat 
2. Robiata Tsania Salsabila 
3. Hafid Zaeny 

## Disclaimer 
Pada challenge kali ini, terdapat dua server yang terpisah yakni pada server api.js dan index.js.  
Pada server API yang digunakan untuk proses CRUD dan mengembalikan data dalam bentuk JSON dengan menggunakan port 5000. 
Sedangkan pada server index.js digunakan untuk menjalankan routing halaman dengan menggunakan port 3000.
Maka agar challenge ini dapat berjalan dengan benar dan semestinya harus menyalakan kedua server tersebut. 

## Pages Route 
```http
Home page   : GET http://localhost:3000/
Filter car  : GET http://localhost:3000/filter
Search car  : GET http://localhost:3000/search 
Add Car     : GET http://localhost:3000/create
Edit by ID  : GET http://localhost:3000/edit/:id
```

## Api
```http
GET all data: http://localhost:5000/api/
GET by ID   : http://localhost:5000/api/:id
POST        : http://localhost:5000/api
PUT         : http://localhost:5000/api/:id
DELETE      : http://localhost:5000/api/:id
``` 

## Table ERD 
![ERD](/public/images/ERD.png)

## Contoh Request Body dan Response Body
### 1) GET all data

```http
  GET http://localhost:5000/api
```

### Response 
```javascript
[
    {
        "id": 1,
        "name": "Avanza",
        "type": "Home Car",
        "price": "450000",
        "image": "1650438068295.jpg",
        "size": "Medium",
        "createdAt": "2022-04-20T06:59:09.790Z",
        "updatedAt": "2022-04-20T07:01:08.572Z"
    },
    {
        "id": 2,
        "name": "Ferrari",
        "type": "Sport Car",
        "price": "950000",
        "image": "ferrari.jpg",
        "size": "Medium",
        "createdAt": "2022-04-20T06:59:09.790Z",
        "updatedAt": "2022-04-20T06:59:09.790Z"
    },
    {
        "id": 3,
        "name": "Hummer",
        "type": "Trail Car",
        "price": "700000",
        "image": "hummer.jpg",
        "size": "Large",
        "createdAt": "2022-04-20T06:59:09.790Z",
        "updatedAt": "2022-04-20T06:59:09.790Z"
    }
]
```

### 2) GET By ID
```http
  GET http://localhost:5000/api/:id
```

| Params    | Type     | Description  |
| :-------- | :------- | :------------|
| `id`      | `Number` | **Required** |


### Response 
```javascript 
[
    {
        "id": 1,
        "name": "Avanza",
        "type": "Home Car",
        "price": "450000",
        "image": "1650438068295.jpg",
        "size": "Medium",
        "createdAt": "2022-04-20T06:59:09.790Z",
        "updatedAt": "2022-04-20T07:01:08.572Z"
    }
]
```

### 3) Insert Data
```http
  POST http://localhost:5000/api/
```

| Body      | Type     | Description  |
| :-------- | :------- | :------------|
| `name`    | `String` | **Required** |
| `type`    | `String` | **Required** |
| `price`   | `String` | **Required** |
| `image`   | `String` | **Required** |
| `size`    | `String` | **Required** |

### Response 
```javascript
[
    {
        "id": 6,
        "name": "Grand Livina",
        "type": "Sports Car",
        "price": "700000",
        "image": "1650591726577.jpg",
        "size": "Small",
        "createdAt": "2022-04-22T01:42:06.623Z",
        "updatedAt": "2022-04-22T01:42:06.625Z"
    }
]
```

### 4) Update data
```http
  PUT http://localhost:5000/api/:id
```

| Params    | Type     | Description  |
| :-------- | :------- | :------------|
| `id`      | `Number` | **Required** |


| Body      | Type     | Description  |
| :-------- | :------- | :------------|
| `name`    | `String` | **Required** |
| `type`    | `String` | **Required** |
| `price`   | `String` | **Required** |
| `image`   | `String` | **Required** |
| `size`    | `String` | **Required** |

## Response 
```javascript
[
    {
    "id": 6,
    "name": "Xenia",
    "type": "Home Car",
    "price": "600000",
    "image": "1650591726577.jpg",
    "size": "Small",
    "createdAt": "2022-04-22T01:42:06.623Z",
    "updatedAt": "2022-04-22T13:28:28.486Z"
}
]
```

### 5) Delete Data
```http
  DELETE http://localhost:5000/api/:id
```

| Params    | Type     | Description  |
| :-------- | :------- | :------------|
| `id`      | `Number` | **Required** |

### Response 
Nothing is returned 



